Webform Booking Calendar
========================

The Webform Booking Calendar module extends the Webform Booking module by adding a calendar block for visualizing bookings within Drupal.

Features:
---------
- Interactive calendar view of Webform Booking submissions.
- Displays booking details such as session duration, customer info, and booking details.
- Fully responsive and user-friendly, powered by FullCalendar.js.
- Supports multiple Webforms and booking elements.
- Permission-based visibility.

Requirements:
-------------
- Webform module
- Webform Booking module
- FullCalendar.js library

Installation:
-------------
1. Install the module as usual.
2. Enable the module via the Drupal UI or Drush: `drush en webform_booking_calendar`.
3. Place the Webform Booking Calendar block on your desired page via the Block Layout settings.
4. Configure the block to specify which Webforms and elements to display in the calendar.

Credits:
--------
Supporting Organization: https://www.drupal.org/my-local-trades
Original author: https://www.drupal.org/u/mylocaltrades
